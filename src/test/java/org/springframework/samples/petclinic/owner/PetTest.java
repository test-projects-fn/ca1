package org.springframework.samples.petclinic.owner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.samples.petclinic.visit.Visit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.AfterEach;


import java.util.*;

import java.time.LocalDate;

class PetTest {

	private static Pet pet;

	private Pet createSomePet() {
		Pet newPet = new Pet();
		PetType dog = new PetType();
		dog.setName("dog");
		newPet.setId(1);
		newPet.setType(dog);
		newPet.setName("Bob");
		LocalDate birthDate = LocalDate.parse("2000-06-13");
		newPet.setBirthDate(birthDate);
		return newPet;
	}

	private Visit createNewVisit(String date) {
		Visit newVisit = new Visit();
		LocalDate newVisitDate = LocalDate.parse(date);
		newVisit.setDate(newVisitDate);
		return newVisit;
	}

	private Collection<Visit> createSomeVisits() {
		Collection<Visit> visits = new ArrayList<>();
		Visit firstVisit = createNewVisit("2012-04-13");
		visits.add(firstVisit);
		Visit secondVisit = createNewVisit("2003-02-20");
		visits.add(secondVisit);
		Visit thirdVisit = createNewVisit("2002-10-10");
		visits.add(thirdVisit);
		return visits;
	}

	@BeforeEach
	public void setup() {
		pet = createSomePet();
		Collection<Visit> visits = createSomeVisits();
		pet.setVisitsInternal(visits);
	}

	@Test
	void getVisitsTest() {
		List<Visit> actualVisits = pet.getVisits();
		Assertions.assertEquals("2012-04-13", actualVisits.get(0).getDate().toString());
		Assertions.assertEquals("2003-02-20", actualVisits.get(1).getDate().toString());
		Assertions.assertEquals("2002-10-10", actualVisits.get(2).getDate().toString());
	}

	@Test
	void getVisitsBetweenTest() {
		LocalDate start = LocalDate.parse("2001-12-15");
		LocalDate end = LocalDate.parse("2004-10-11");
		List<Visit> actualVisits = pet.getVisitsBetween(start, end);
		Assertions.assertEquals(2, actualVisits.size());
		Assertions.assertEquals("2003-02-20" , actualVisits.get(0).getDate().toString());
		Assertions.assertEquals("2002-10-10" , actualVisits.get(1).getDate().toString());
	}

	@Test
	void getVisitsUntilAgeTest() {
		List<Visit> actualVisits1 = pet.getVisitsUntilAge(5);
		Assertions.assertEquals(2, actualVisits1.size());
		Assertions.assertEquals("2003-02-20" , actualVisits1.get(0).getDate().toString());
		Assertions.assertEquals("2002-10-10" , actualVisits1.get(1).getDate().toString());

		List<Visit> actualVisits2 = pet.getVisitsUntilAge(13);
		Assertions.assertEquals(3, actualVisits2.size());
		Assertions.assertEquals("2012-04-13" , actualVisits2.get(0).getDate().toString());
		Assertions.assertEquals("2003-02-20" , actualVisits2.get(1).getDate().toString());
		Assertions.assertEquals("2002-10-10" , actualVisits2.get(2).getDate().toString());
	}

	@Test
	void removeVisitTest() {
		Visit toBeRemovedVisit = new Visit();
		toBeRemovedVisit = pet.getVisits().get(1);
		pet.removeVisit(toBeRemovedVisit);
		List<Visit> actualVisits = pet.getVisits();
		Assertions.assertEquals(2, actualVisits.size());
		Assertions.assertEquals("2012-04-13" , actualVisits.get(0).getDate().toString());
		Assertions.assertEquals("2002-10-10" , actualVisits.get(1).getDate().toString());
	}

	@Test
	void addVisitTest() {
		Visit toBeAddedVisit = new Visit();
		LocalDate toBeAddedVisitDate = LocalDate.parse("2016-03-03");
		toBeAddedVisit.setDate(toBeAddedVisitDate);
		pet.addVisit(toBeAddedVisit);
		Assertions.assertTrue(pet.getVisits().contains(toBeAddedVisit));
	}

	@AfterEach
	public void tearDown() {
		pet = null;
	}

}
