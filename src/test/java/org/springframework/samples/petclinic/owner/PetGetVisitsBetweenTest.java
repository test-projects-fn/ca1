package org.springframework.samples.petclinic.owner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.samples.petclinic.visit.Visit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assume.assumeTrue;

@RunWith(Theories.class)
public class PetGetVisitsBetweenTest {

	private static Pet pet;

	@DataPoints
	public static Visit[] visits = {
		createNewVisit("2012-04-13"),
		createNewVisit("2003-02-20"),
		createNewVisit("2002-10-10"),
		createNewVisit("2008-11-14"),
		createNewVisit("2011-08-19"),
		createNewVisit("2015-03-08"),
		createNewVisit("2009-09-25")
	};

	@DataPoints
	public static Visit[] visits2 = {
		createNewVisit("2012-04-13"),
		createNewVisit("2003-02-20"),
		createNewVisit("2002-10-10"),
		createNewVisit("2008-11-14"),
		createNewVisit("2011-08-19"),
		createNewVisit("2015-03-08"),
		createNewVisit("2009-09-25")
	};

	private static Visit createNewVisit(String date) {
		Visit newVisit = new Visit();
		LocalDate newVisitDate = LocalDate.parse(date);
		newVisit.setDate(newVisitDate);
		return newVisit;
	}

	private Pet createSomePet() {
		Pet newPet = new Pet();
		PetType dog = new PetType();
		dog.setName("dog");
		newPet.setId(1);
		newPet.setType(dog);
		newPet.setName("Bob");
		LocalDate birthDate = LocalDate.parse("2000-06-13");
		newPet.setBirthDate(birthDate);
		return newPet;
	}

	private Collection<Visit> createSomeVisits() {
		Collection<Visit> _visits = new ArrayList<>();
		for(int i = 0; i < PetGetVisitsBetweenTest.visits.length; i++) {
			_visits.add(PetGetVisitsBetweenTest.visits[i]);
		}
		return _visits;
	}

	@Before
	public void setup() {
		pet = createSomePet();
		Collection<Visit> _visits = createSomeVisits();
		pet.setVisitsInternal(_visits);
	}

	@Theory public void testGetVisitsBetween (Visit v1, Visit v2) { // Parameters!
		assumeTrue (v1 != null); // Assume
		assumeTrue (v1.getDate() != null); // Assume
		assumeTrue (v2 != null); // Assume
		assumeTrue (v2.getDate() != null); // Assume
		assumeTrue(v1.getDate().isBefore(v2.getDate())); // Assume
		List<Visit> petVisits = pet.getVisitsBetween(v1.getDate(), v2.getDate()); // Act
		for(int i = 0; i < PetGetVisitsBetweenTest.visits.length; i++) {
			if(PetGetVisitsBetweenTest.visits[i].getDate().isAfter(v1.getDate())
			&& PetGetVisitsBetweenTest.visits[i].getDate().isBefore(v2.getDate())) {
				assertTrue (petVisits.contains(PetGetVisitsBetweenTest.visits[i])); // Assert
			}
		}
	}

	@After
	public void tearDown() {
		pet = null;
	}
}
