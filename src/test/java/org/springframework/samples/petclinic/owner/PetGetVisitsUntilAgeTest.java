package org.springframework.samples.petclinic.owner;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.samples.petclinic.visit.Visit;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class PetGetVisitsUntilAgeTest {

	private static Pet pet;
	private int untilAge;
	private LocalDate birthDate;
	private ArrayList<Visit> visits, validVisits;

	public PetGetVisitsUntilAgeTest (int _age, LocalDate _birthDate, ArrayList<Visit> _visits, ArrayList<Visit> _validVisits)
	{
		this.untilAge = _age;
		this.birthDate = _birthDate;
		this.visits = _visits;
		this.validVisits = _validVisits;
	}

	private static Visit createNewVisit(String date) {
		Visit newVisit = new Visit();
		LocalDate newVisitDate = LocalDate.parse(date);
		newVisit.setDate(newVisitDate);
		return newVisit;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> parameters()
	{
		ArrayList<Visit> paraVisits1 = new ArrayList<>();
		paraVisits1.add(createNewVisit("2004-04-13"));
		paraVisits1.add(createNewVisit("2002-06-21"));
		paraVisits1.add(createNewVisit("2008-11-24"));
		ArrayList<Visit> paraValids1 = new ArrayList<>();
		paraValids1.add(paraVisits1.get(0));
		paraValids1.add(paraVisits1.get(1));

		ArrayList<Visit> paraVisits2 = new ArrayList<>();
		paraVisits2.add(createNewVisit("2014-08-15"));
		paraVisits2.add(createNewVisit("2011-09-27"));
		ArrayList<Visit> paraValids2 = new ArrayList<>();
		paraValids2.add(paraVisits2.get(1));

		return Arrays.asList (new Object [][] {
			{5, LocalDate.parse("2000-01-01"), paraVisits1, paraValids1},
			{3, LocalDate.parse("2010-06-20"), paraVisits2, paraValids2}
		});
	}

	private Pet createSomePet(LocalDate birthDate) {
		Pet newPet = new Pet();
		PetType dog = new PetType();
		dog.setName("dog");
		newPet.setId(1);
		newPet.setType(dog);
		newPet.setName("Jigar");
		newPet.setBirthDate(birthDate);
		return newPet;
	}

	@Before
	public void setup() {
		pet = createSomePet(birthDate);
		pet.setVisitsInternal(visits);
	}

	@Test
	public void getVisitsUntilAge() {
		assertTrue ("getVisitsUntilAge Size Test",
			validVisits.size() == pet.getVisitsUntilAge(untilAge).size());
		for(int i = 0; i < validVisits.size(); i++) {
			assertTrue("getVisitsUntilAge Membership Test", pet.getVisitsUntilAge(untilAge)
				.contains(validVisits.get(i)));
		}
	}

	@After
	public void tearDown() {
		pet = null;
	}
}
